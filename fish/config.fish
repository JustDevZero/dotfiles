#
#       The MIT License
#
#       Copyright (c) Daniel Ripoll, <info@danielripoll.es>, <http://danielripoll.es>
#
#       Permission is hereby granted, free of charge, to any person obtaining a copy
#       of this software and associated documentation files (the "Software"), to deal
#       in the Software without restriction, including without limitation the rights
#       to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#       copies of the Software, and to permit persons to whom the Software is
#       furnished to do so, subject to the following conditions:
#
#       The above copyright notice and this permission notice shall be included in
#       all copies or substantial portions of the Software.
#
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#       THE SOFTWARE.

set -x PROJECTS ~/Projects
set -x FISH $HOME/.dotfiles
set -x DOTFILES_VERSION (cat $FISH/VERSION)

function check_dotfiles_version
    wget -q -t 1 --timeout 5 'https://gitlab.com/JustDevZero/dotfiles/-/tags?format=atom'  -O ~/.tmpfile.xml
    set remote_version (awk -F "[<id>]" '/ <id>/' {getline; print $3;exit} ~/.tmpfile.xml|head -n1|tr -d '<id>'|sed s'=/= ='g|awk '{print $(NF)}')
    if test "$remote_version" -gt "$DOTFILES_VERSION";
        echo "Ey, there is a new dotfiles version, check it out. at\n https://gitlab.com/JustDevZero/dotfiles/"
    end
    rm ~/.tmpfile.xml 2>/dev/null
    set -e remote_version
end

if test -n (which wget 2>/dev/null)
    if test -f ~/.dotfiles_lastcheck
        set curr_date (date +%Y%m%d%H%M)
        set last_date (cat ~/.dotfiles_lastcheck 2>/dev/null||echo $curr_date)
        set difference_date (math $last_date - $curr_date)
        if test $difference_date -gt 120
            check_dotfiles_version
            rm -rf ~/.dotfiles_lastcheck
            echo (date +%Y%m%d%H%M) > ~/.dotfiles_lastcheck
        else
            check_dotfiles_version
            echo (date +%Y%m%d%H%M) > ~/.dotfiles_lastcheck
            set -e curr_date
            set -e last_date
            set -e difference_date
        end
    end
end

# Stash your environment variables in ~/.local.fish. This means they'll stay out
# of your main dotfiles repository (which may be public, like this one), but
# you'll have access to them in your scripts.

if test -f ~/.local.fish
  source ~/.local.fish
end

if test (id -u) -eq '0'
    set -x IFSUDO ""
else
    set -x IFSUDO "sudo"
end